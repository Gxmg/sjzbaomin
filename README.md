<a name="bc4f21f4"></a>
# 1 简介
 	该文档为微信小程序开发说明文档。<br />模板【demo_applet】项目界面部分包括登录弹窗获取用户信息和手机号信息和主页界面。<br />其中主页界面展示了轮播图组件的使用，滚动视图上拉刷新、触底查询列表， 地图展示点 线面。<br />可以检测新版本，强制用户更新小程序。
<a name="27aedc3f"></a>
# 2 代码目录
```
|-- api               //接口方法文件
|   |-- login.js
|-- components  //公共组件
|   |--loginbox  //登录弹窗组件
|   |--navbar //头部导航条组件
|   |--	map_bak.vue //地图组件
|-- node_modules  // 存放用包管理工具下载安装的包的文件夹
|-- pages   //所有的页面存放目录,和vue的view文件夹差不多，
|   |-- home  //主页界面，包括登录获取权限等
|   |   |-- index.vue
|   |-- mine  //个人中心界面
|   |   |-- index.vue
|-- static   //放置静态文件，一般放些图片文件
|   |-- images  //放图片
|   |-- global.config //全局变量配置
|-- store   //	状态管理文件夹
|-- styles  //样式文件
|   |-- index.scss  //全局样式文件
|   |-- vant-theme.scss  //vant组件样式全局样式文件
|-- uni_modules //uni组件存放文件夹
|-- utils  //公共工具目录
|   |-- date.js  //处理时间的方法
|   |-- request.js // 接口请求方法封装
|   |-- requestImage.js //请求图片的方法
|   |-- wxshare.js //分享小程序的方法
|-- wxcomponents  //微信组件的存放文件夹 如vant
|-- .gitignore //上传忽略的文件
|-- App.vue  //根组件，所有页面都是在App.vue下进行切换的，是页面入口文件，可以调用应用的生命周期函数。检测版本，强制更新小程序
|--index.html  //页面入口
|-- main.js    //项目入口文件，主要作用是初始化vue实例并使用需要的插件。
|-- manifest.json // 文件是应用的配置文件，用于指定应用的名称、图标、权限等。
|-- package-lock.json  //记录了node_modules目录下所有模块（包）的名称、版本号、下载地址
|-- package.json  //记录了下载的依赖
|-- pages.json  /小程序项目的全局配置文件，定义页面文件的路径、窗口样式、原生的导航栏、底部的原生tabbar 等
|-- project.config.json //项目的配置文件，用来记录对小程序开发工具所做的个性化配置
|-- project.private.config.json //团队成员使用 project.private.config.json 作为个人配置（此配置文件会优先使用）
|-- uni.scss  //文件的用途是为了方便整体控制应用的风格。比如按钮颜色、边框风格，uni.scss文件里预置了一批scss变量预置。
```
<a name="be41ce5e"></a>
# 3 运行、调试代码
项目在HBuilder 中编辑，配合软件【微信开发者工具】运行、调试。<br />HBuilder安装配置软件【微信开发者工具】方法参考教程：https://blog.csdn.net/qq_43432158/article/details/12384255/7。
<a name="nGdfA"></a>
## 3.1微信开发者工具中调试
配置完成后启动项目步骤：HBuilder->运行->运行到小程序模拟器->微信开发者工具，即可在微信开发者工具中打开项目。在微信开发者工具中可以和浏览器中一样调试代码，也可参照官网教程：[https://uniapp.dcloud.net.cn/quickstarthx.html#%E8%BF%90%E8%A1%8Cuni-app](https://uniapp.dcloud.net.cn/quickstart-hx.html#%E8%BF%90%E8%A1%8Cuni-app)
<a name="hUng3"></a>
## ![image.png](https://cdn.nlark.com/yuque/0/2022/png/1217005/1669944765130-8ab0328a-be88-4258-a017-ba81df3381df.png#clientId=ua10df829-73aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=635&id=u4aaa0320&margin=%5Bobject%20Object%5D&name=image.png&originHeight=635&originWidth=964&originalType=binary&ratio=1&rotation=0&showTitle=false&size=75998&status=done&style=none&taskId=udb7e45ac-59e5-4e00-8cba-ac52afc3924&title=&width=964)<br />3.2真机调试
可点击【预览】，开发者工具会自动打包当前项目，并上传小程序代码至微信的服务器，成功之后会在界面上显示一个二维码。使用当前小程序开发者的微信扫码即可看到小程序在手机客户端上的真实表现。<br />![image.png](https://cdn.nlark.com/yuque/0/2022/png/1217005/1669962137515-2ac67f1a-0056-49ce-9f13-5b622ac5b626.png#clientId=ua10df829-73aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=1011&id=u66330d13&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1011&originWidth=1895&originalType=binary&ratio=1&rotation=0&showTitle=false&size=381297&status=done&style=none&taskId=u213ab2db-2d95-47df-891f-29f3fd9ec06&title=&width=1895)<br />如果代码包体积较大，调试前，按照如下步骤操作【详情】->【本地设置】->【预览及真机.......】<br />![image.png](https://cdn.nlark.com/yuque/0/2022/png/1217005/1669962888289-ac1bcd27-e02a-4ac7-8302-24f3532bbddd.png#clientId=ua10df829-73aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=1029&id=ue2f23bb1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1029&originWidth=1910&originalType=binary&ratio=1&rotation=0&showTitle=false&size=158554&status=done&style=none&taskId=u89fb131e-2bfd-47a8-8fe3-4066f2f9036&title=&width=1910)
<a name="496f2d26"></a>
# 4 开发完成部署
<a name="cjUpM"></a>
## 4.1分包
        当代码量较大时，小程要发布时会受到大小的限制，需要进行分包操作。参考文档：[[https://blog.csdn.net/qq_54714468/article/details/121602026](https://blog.csdn.net/qq_54714468/article/details/121602026)]
<a name="DgM9n"></a>
## 4.2部署
一个小程序从开发完到上线一般要经过 预览-> 上传代码 -> 提交审核 -> 发布等步骤<br />（1）**预览** <br />即标题3，运行代码<br />（2）**上传代码**，<br />用于提交体验或者审核使用的。点击开发者工具顶部操作栏的上传按钮，填写版本号以及项目备注，需要注意的是，这里版本号以及项目备注是为了方便管理员检查版本使用的，开发者可以根据自己的实际要求来填写这两个字段。<br />![image.png](https://cdn.nlark.com/yuque/0/2022/png/1217005/1669964092076-7c647741-b80a-4de6-b473-49d2b600667f.png#clientId=ua10df829-73aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=608&id=ue082ad46&margin=%5Bobject%20Object%5D&name=image.png&originHeight=608&originWidth=1493&originalType=binary&ratio=1&rotation=0&showTitle=false&size=68770&status=done&style=none&taskId=u6fe4664e-72bd-4188-9a90-7ba44dfc3d6&title=&width=1493)<br />上传成功之后，登录[小程序管理后台](https://mp.weixin.qq.com/) - 版本管理 - 开发版本 就可以找到刚提交上传的版本了。<br />小程序后台登录网址：[https://mp.weixin.qq.com/](https://mp.weixin.qq.com/)<br />![image.png](https://cdn.nlark.com/yuque/0/2022/png/1217005/1669964324939-8f836269-278d-446d-8d5d-c335604098c2.png#clientId=ua10df829-73aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=924&id=u8f64b2d3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=924&originWidth=1853&originalType=binary&ratio=1&rotation=0&showTitle=false&size=89229&status=done&style=none&taskId=u6eb3336f-bba4-4529-9dc5-5c418886b79&title=&width=1853)<br />可以将这个版本设置 体验版 或者是 提交审核<br />（3）**提交审核：**<br />为了保证小程序的质量，以及符合相关的规范，小程序的发布是需要经过审核的。<br />在开发者工具中上传了小程序代码之后，登录 [小程序管理后台](https://mp.weixin.qq.com/) - 版本管理 - 开发版本 找到提交上传的版本。<br />在开发版本的列表中，点击 **提交审核** 按照页面提示，填写相关的信息，即可以将小程序提交审核。<br />需要注意的是，**请开发者严格测试了版本之后，再提交审核**， 过多的审核不通过，可能会影响后续的时间<br />![image.png](https://cdn.nlark.com/yuque/0/2022/png/1217005/1669964608358-bfb1faf9-e861-45cc-95a4-07eb963499ba.png#clientId=ua10df829-73aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=900&id=ue030cf2a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=900&originWidth=1878&originalType=binary&ratio=1&rotation=0&showTitle=false&size=94062&status=done&style=none&taskId=u411d3397-8280-42f2-8397-33d83d6f265&title=&width=1878)<br />**（4）发布：**审核通过后，还需要发布，发布完成后，用户就可以直接看到，模板系统代码中做了强制用户更新小程序，发布后，用户小程序为最新版本<br /> 审核通过之后，管理员的微信中会收到小程序通过审核的通知，此时登录 [小程序管理后台](https://mp.weixin.qq.com/) - 开发管理 - 审核版本中可以看到通过审核的版本。<br />点击发布后，即可发布小程序。小程序提供了两种发布模式：全量发布和分阶段发布。全量发布是指当点击发布之后，所有用户访问小程序时都会使用当前最新的发布版本。分阶段发布是指分不同时间段来控制部分用户使用最新的发布版本，分阶段发布我们也称为灰度发布。一般来说，普通小程序发布时采用全量发布即可，当小程序承载的功能越来越多，使用的用户数越来越多时，采用分阶段发布是一个非常好的控制风险的办法
<a name="0eedd2b9"></a>
# 5 注意事项

1. 小程序包大小限制问题：想方设法缩小包体积，如：静态图片资源迁移到服务器，或者按照文档提供的方案：进行分包。
2. 小程序需要获取手机权限时 如位置信息，需要在manifest.json中配置
```
	"mp-weixin": {
		"appid": "wx0efe76a0c7bd6b1d",
		"setting": {
			"urlCheck": false,
			"postcss": false,
			"es6": true,
			"minified": false
		},
		"permission": {
			"scope.userLocation": {
				"desc": "你的位置信息将用于小程序位置接口的效果展示"
			}
		},
		"requiredPrivateInfos": ["getLocation", "startLocationUpdate", "onLocationChange",
			"startLocationUpdateBackground"
		],
		"requiredBackgroundModes": ["location"], //作用是为了设置页多出“使用小程序期间和离开小程序后”的选项
		"usingComponents": true,
		"optimization": {
			"subPackages": true
		},
		"lazyCodeLoading": "requiredComponents"
	},
```
