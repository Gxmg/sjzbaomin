// 引入工具类
import request from '@/utils/request.js'
// get openid 和 token
export function getTokenOpenId(wxCode) {
	return request({
		url: "/api/wx/login?id=1&code=" + wxCode,
		method: "get",
	})
}

// 比赛报名，type 1 个人，2 志愿者，3 单位
export function gameBaoming(data) {
	return request({
		url: "/api/wx/bm",
		method: "post",
		data: data,
		// header: {
		// 	'Content-Type': 'application/x-www-form-urlencoded' //自定义请求头信息
		// }
	})
}
// 单位报名
export function DanweiBaoming(data) {
	return request({
		url: "/api/wx/dwbm",
		method: "post",
		data: data,
	})
}
// 获取大区
export function getDaqu() {
	return request({
		url: "/api/wx/community",
		method: "get",
	})
}

// 根据大区获取社区
export function getCommunity(data) {
	return request({
		url: "/api/wx/community",
		method: "post",
		data: data,
		// header: {
		// 	'Content-Type': 'application/x-www-form-urlencoded' //自定义请求头信息
		// }
	})
}
// 支付接口
export function payment(data) {
	return request({
		url: "/api/wx/payment",
		method: "post",
		data: data,
		// header: {
		// 	'Content-Type': 'application/x-www-form-urlencoded' //自定义请求头信息
		// }
	})
}
