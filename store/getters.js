const getters = {
	userInfos: state => state.app.userInfos,
	xunheList: state => state.app.xunheList,
	isXunhePage: state => state.app.isXunhePage,
	xunheTime: state => state.app.xunheTime,
	xunheLenth: state => state.app.xunheLenth,
};
export default getters;
