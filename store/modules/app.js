import {
	howLong,
	dateDifference
} from '../../utils/index.js'
const state = {
	userInfos: {},
	xunheList: [{
		points: [],
		width: 1,
		dottedLine: true,
		arrowLine: false,
		color: '#db4f00'
	}],
	ifQualified: false, // 是否合格
	xunheTime: '0天0小时0分钟', // 巡河时间
	xunheLength: 0, // 累计巡河距离
	isXunhePage: false, // 是否在巡河当前页面
	xunheCode: null, // 单次巡河code
	xunheCount: 0, // 控制巡河调用频率，8s调用一次
};

const mutations = {
	set_userInfos: (state, userInfos) => {
		state.userInfos = userInfos;
	},
	set_xunheList: (state, location) => {
		// console.log('点位坐标', location)
		// console.log('点位坐标 类型', location instanceof Array)
		// console.log('!location', !location)
		// console.log('state.xunheCount', state.xunheCount)
		if (!location) return false
		if (location instanceof Array && location.length === 0) return false
		// 控制位置更新频率
		if (state.xunheCount > 3) state.xunheCount = 0;
		if (state.xunheCount === 0) {
			// console.log('#####打点', state.xunheCount)
			state.xunheList[0].points.push(location);
			console.log('总数据', state.xunheList[0].points)
		}
		state.xunheCount++;
		// state.xunheList[0].points.push(location);
		// console.log('总数据', state.xunheList[0].points)
	},
	// 重置总巡河数组
	reset_xunheList: (state, loacation) => {
		state.xunheList[0].points = [];
	},
	set_isXunhePage: (state, isXunhePage) => {
		state.isXunhePage = isXunhePage;
	},
	/**
	 * @xunheTime 巡河的开始时间 2022-09-02 18:12:10
	 */
	set_xunheTime: (state, xunheTime) => {
		const result = howLong(new Date(), new Date(xunheTime.replace(/-/g, "/")));
		state.xunheTime = result.text;
		state.ifQualified = result.minutes >= 5 && state.xunheLength >= 200
		console.log('巡河时间', state.ifQualified)
	},
	/**
	 * @xunheLength 累计巡河距离
	 */
	set_xunheLength: (state, xunheLength) => {
		state.xunheLength = xunheLength;
	},
	/**
	 * @xunheCode 本次巡河code标识，唯一，用于后端异常退出巡河，清楚缓存使用
	 */
	set_xunheCode: (state, xunheCode) => {
		state.xunheCode = xunheCode;
	},
	// 重置巡河累计时间
	reset_xunheTime() {
		state.xunheTime = '0天0小时0分钟'
		state.ifQualified = false
	},
	// 重置巡河累计长度 单位m
	reset_xunheLength() {
		state.xunheLength = 0
	},
	reset_xunheCount() {
		state.xunheCount = 0
	}
};

const actions = {
	set_userInfos_fun({
		commit
	}, userInfos) {
		commit('set_userInfos', userInfos);
	},
	set_xunheList_fun({
		commit
	}, xunheList) {
		commit('set_xunheList', xunheList);
	},
	set_isXunhePage_fun({
		commit
	}, isXunhePage) {
		commit('set_isXunhePage', isXunhePage);
	},

	set_xunheTime_fun({
		commit
	}, xunheTime) {
		commit('set_xunheTime', xunheTime);
	},
	set_xunheLength_fun({
		commit
	}, xunheLength) {
		commit('set_xunheLength', xunheLength);
	}

};

export default {
	namespaced: true,
	state,
	mutations,
	actions
};
