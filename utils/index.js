export function goPage(path) {
	uni.navigateTo({
		url: path
	})
}
export function goTab(path) {
	uni.switchTab({
		url: path
	})
}
