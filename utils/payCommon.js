import {
	payment
} from '@/api/login.js'
const payCommonAction = (price, callback) => {
	let openid = wx.getStorageSync('openid')
	payment({
		openid: openid,
		price: price
	}).then(info_dict => {
		uni.requestPayment({
			provider: 'wxpay', // 服务提提供商
			timeStamp: info_dict.timeStamp, // 时间戳
			nonceStr: info_dict.nonceStr, // 随机字符串
			package: info_dict.package,
			signType: info_dict.signType, // 签名算法
			paySign: info_dict.paySign, // 签名
			success: (res) => {
				callback()
			},
			fail: (err) => {
				uni.showToast({
					title: '取消支付'
				})
			}
		});
	})

}
export {
	payCommonAction
}