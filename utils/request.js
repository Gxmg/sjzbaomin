import {
	g_config
} from '@/static/global.config.js'
import {
	loginToken,
} from '@/api/login.js'
var flag = false // 弹框标示
function req(obj) {
	return new Promise((resolve, reject) => {
		let method = obj.method || "GET";
		let url = g_config.baseUrl + obj.url || "";
		let data = obj.data || {};
		let header = obj.header || {
			'Content-Type': obj.contentType || 'application/json',
		};
		let token = uni.getStorageSync('token') || undefined
		if (token) {
			header['token'] = token
		}
		let success = obj.success; // 成功回调函数
		let fail = obj.fail; //表示失败后，要执行的回调函数
		uni.request({
			url: url,
			data: data,
			method: method,
			header: header,
			success: ((res) => {
				console.log('接口的返回', res)
				if (res.statusCode == 503 || res.statusCode == 500) {
					uni.showToast({
						icon: 'error',
						title: '服务端错误',
						duration: 2000
					})
					reject(res.statusCode)
				}
				if (res.statusCode == 200) {
					resolve(res.data)
				} else {
					uni.showToast({
						icon: 'none',
						position: 'top',
						title: res.data.msg, // 提示错误
						duration: 2000
					})
					return
				}
			}),
			fail: ((err) => {
				console.log('错误方法')
				reject(err)
			})
		})
	})

}

function getPageUrl() {
	const pages = getCurrentPages();
	console.log('pages', pages)
	if (pages.length === 1) {
		return pages[0].$page.fullPath
	} else {
		return pages[1].$page.fullPath
	}
}

export default req